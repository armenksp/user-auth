<?php

namespace App\Components;

use GuzzleHttp\Client;

class ImportDataAddress
{
    public $client;
    public $term;
    public $top;

    /**
     * @param $client
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => env('ADDRESS_URL') . 'autocomplete/' . $this->term . '?api-key=' . env('API_KEY') . '&top' . $this->top,
            'timeout' => 2.0,
            'verify' => false
        ]);
    }

}
