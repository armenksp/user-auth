<?php

namespace App\Components;

use GuzzleHttp\Client;

class ImportDataAddressById
{
    public $client;
    public $id;

    /**
     * @param $client
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => env('ADDRESS_URL') . 'get/' . $this->id . '?api-key=' . env('API_KEY'),
            'timeout' => 2.0,
            'verify' => false
        ]);
    }

}
