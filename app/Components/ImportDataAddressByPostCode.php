<?php

namespace App\Components;

use GuzzleHttp\Client;

class ImportDataAddressByPostCode
{
    public $client;
    public $postCode;

    /**
     * @param $client
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => env('ADDRESS_URL') . 'find/' . $this->postCode . '?api-key=' . env('API_KEY'),
            'timeout' => 2.0,
            'verify' => false
        ]);
    }

}
