<?php

namespace App\Console\Commands;

use App\Models\Address;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class ImportJsonAddressByIdCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:jsonaddressbyid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get address by Id';

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        // TODO will return a random address
        $address = Address::inRandomOrder()->first();
        $response = Http::get(env('ADDRESS_URL') . 'get/' . $address['address_id'] . '?api-key=' . env('API_KEY'));
        dd($response->json());

    }
}
