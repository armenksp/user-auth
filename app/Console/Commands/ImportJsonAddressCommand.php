<?php

namespace App\Console\Commands;

use App\Models\Address;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class ImportJsonAddressCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:jsonaddress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Data from Address.io';

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $insertData = [];
        $response = Http::get(env('ADDRESS_URL') . 'autocomplete/{term}?api-key=' . env('API_KEY'));
        $data = $response->json();


        foreach ($data as $value) {

            foreach ($value as $item) {
                $insertData[] = [
                    'address' => $item['address'],
                    'url' => $item['url'],
                    'address_id' => $item['id'],
                ];
            }
        }
        Address::insert($insertData);
    }
}
