<?php

namespace App\Console\Commands;

use App\Models\User;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class ImportJsonPlaceholderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:jsonplaceholder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get data from json placeholder';

    /**
     * Execute the console command.
     *
     * @throws GuzzleException
     */
    public function handle()
    {
        // TODO first option
//        $import  = new ImportDataClient();
//        $response = $import->client->request('GET', 'users');


        // TODO second option
        $response = Http::get(env('SERVICE_URL') . '/users');
        $data = json_decode($response->getBody()->getContents());

        $userData = [];
        foreach ($data as $item) {
            $userData[] = [
                'name' => $item->name,
                'email' => $item->email,
                'phone_number' => 777777,
                'password' => Hash::make(12345678),
            ];
        }
        User::insert($userData);
    }
}
