<?php

namespace App\Http\Controllers;

use App\Http\Requests\AllAddressRequest;
use App\Http\Requests\SingleAddressRequest;
use App\Services\AddressService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Throwable;

class AddressController extends Controller
{

    public function getAddress(AllAddressRequest $request): bool|JsonResponse
    {
        try {
            return response()->json(['status' => 200, 'message' => __('messages.get_address'),
                'data' => (new AddressService)->getAddress($request->all())]);
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }

    public function getAddressById(SingleAddressRequest $request): bool|JsonResponse
    {
        try {
            return response()->json(['status' => 200, 'message' => __('messages.get_address'),
                'data' => (new AddressService)->getAddressById($request['id'])]);
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }

    public function getAddressByPostCode(Request $request)
    {
        try {
            return response()->json(['status' => 200, 'message' => __('messages.get_address'),
                'data' => (new AddressService)->getAddressByPostCode($request['postCode'])]);
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }
}
