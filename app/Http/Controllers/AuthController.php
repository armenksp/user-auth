<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use Illuminate\Http\JsonResponse;
use Throwable;
use App\Services\AuthService;

class AuthController extends Controller
{

    private AuthService $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function createUser(RegistrationRequest $request): bool|JsonResponse
    {
        try {
            $userData = $request->all();
            $user = $this->authService->createUser($userData);
            if ($user) {
                $user->sendEmailVerificationNotification();
                return response()->json(['data' => $user, 'message' => __('messages.success'), 'status' => 200]);
            }
            return response()->json(['status' => 404, 'message' => __('messages.Something went wrong'), 'data' => []]);

        } catch (Throwable $e) {
            report($e);
            return false;
        }
    }

    public function login(LoginRequest $request): bool|JsonResponse
    {
        try {
            return $this->authService->loginUser($request->all());

        } catch (Throwable $e) {
            report($e);
            return false;
        }
    }


}
