<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteOrderRequest;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Services\OrderService;
use Illuminate\Http\JsonResponse;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;
use Throwable;
use App\Jobs\sendMessage;

class OrdersController extends Controller
{

    private OrderService $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }


    public function createOrder(OrderRequest $request): bool|JsonResponse
    {
        try {
            $result = $this->orderService->createOrder($request->all());
            return response()->json(['data' => $result, 'message' => __('messages.Success'), 'status' => 200]);

        } catch (Throwable $e) {
            report($e);
            return false;
        }
    }

    public function updated(UpdateOrderRequest $request): bool|JsonResponse
    {
        try {
            $result = $this->orderService->updateOrder((int)$request->input('orderId'));
            if ($result) {
                // TODO send Email after shipment
                Dispatch(new SendMessage('send email'))->delay(now()->addMinute(10));
                return response()->json(['data' => $result, 'message' => __('messages.Order updated Successful'), 'status' => 200]);
            }
            return response()->json(['status' => 404, 'message' => __('messages.Something went wrong'), 'data' => []]);

        } catch (Throwable $e) {
            report($e);
            return false;
        }
    }

    public function delete(DeleteOrderRequest $id): bool|JsonResponse
    {
        try {
            $result = $this->orderService->deleteOrder($id);
            if ($result) {
                return response()->json(['data' => $result, 'message' => __('messages.Order deleted Successful'), 'status' => 200]);
            }
            return response()->json(['status' => 404, 'message' => __('messages.Something went wrong'), 'data' => []]);

        } catch (Throwable $e) {
            report($e);
            return false;
        }

    }

    public function showOrders(): bool|JsonResponse
    {
        try {
            $result = $this->orderService->getAllOrders();
            if ($result) {
                return response()->json(['data' => $result, 'message' => __('messages.Successful'), 'status' => 200]);
            }
            return response()->json(['status' => 404, 'message' => __('messages.Something went wrong'), 'data' => []]);

        } catch (Throwable $e) {
            report($e);
            return false;
        }
    }
}
