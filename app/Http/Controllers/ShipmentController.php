<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShipmentRequest;
use App\Services\ShipmentService;
use Illuminate\Http\JsonResponse;


class ShipmentController extends Controller
{
    private ShipmentService $shipmentService;

    public function __construct(ShipmentService $shipmentService)
    {
        $this->shipmentService = $shipmentService;
    }

    public function shipment(ShipmentRequest $request): bool|JsonResponse
    {
        try {

            $result = $this->shipmentService->shipment($request->all());
            return response()->json(['data' => $result, 'message' => __('messages.Success'), 'status' => 200]);

        } catch (\Exception $e) {
            report($e);
            return false;
        }
    }
}
