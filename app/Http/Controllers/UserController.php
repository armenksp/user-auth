<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateRequest;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Throwable;

class UserController extends Controller
{

    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getUsers(): bool|JsonResponse
    {
        try {
            $result = $this->userService->getAllUsers();
            return response()->json(['data' => $result, 'message' => __('messages.Success'), 'status' => 200]);

        } catch (\Exception $e) {
            report($e);
            return false;
        }

    }

    public function updateUser(UpdateRequest $request): bool|JsonResponse
    {
        try {
            $userData = $request->all();
            $updatedData = $this->userService->updateUser($userData);
            return response()->json(['data' => $updatedData, 'message' => __('messages.Success'), 'status' => 200]);
        } catch (Throwable $e) {
            report($e);
            return false;
        }

    }
}
