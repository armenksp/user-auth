<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    public function verify($userId, Request $request): JsonResponse|RedirectResponse
    {
        if (!$request->hasValidSignature()) {
            return response()->json(['message' => __('messages.Invalid/Expired url provided.'), 'status' => 401]);
        }

        $user = User::findOrFail($userId);

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
        }

        return redirect()->to('/');
    }

    public function resend(): JsonResponse
    {
        if (auth()->user()->hasVerifiedEmail()) {
            return response()->json(['message' => __('messages.Email already verified.'), 'status' => 200]);
        }

        auth()->user()->sendEmailVerificationNotification();
        return response()->json(['message' => __('messages.Email verification link sent on your email id'), 'status' => 200]);
    }
}
