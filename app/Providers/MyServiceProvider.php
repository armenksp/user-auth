<?php

namespace App\Providers;

use App\Models\Address;
use App\Models\Order;
use App\Models\Shipment;
use App\Models\User;
use App\Repository\AddressRepository;
use App\Repository\AuthRepository;
use App\Repository\OrderRepository;
use App\Repository\ShipmentRepository;
use App\Repository\UserRepository;
use App\Services\AddressService;
use App\Services\AuthService;
use App\Services\Interfaces\AddressServiceInterface;
use App\Services\Interfaces\AuthServiceInterface;
use App\Services\Interfaces\OrderServiceInterface;
use App\Services\Interfaces\ShipmentServiceInterface;
use App\Services\Interfaces\UserServiceInterface;
use App\Services\OrderService;
use App\Services\ShipmentService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class MyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->app->bind(ShipmentServiceInterface::class, function ($app) {
            return new ShipmentService(new ShipmentRepository(new Shipment()));
        });

        $this->app->bind(OrderServiceInterface::class, function ($app) {
            return new OrderService(new OrderRepository(new Order()));
        });

        $this->app->bind(AuthServiceInterface::class, function ($app) {
            return new AuthService(new AuthRepository(new User()));
        });

        $this->app->bind(UserServiceInterface::class, function ($app) {
            return new UserService(new UserRepository(new User()));
        });

        $this->app->bind(AddressServiceInterface::class, function ($app) {
            return new AddressService(new AddressRepository(new Address()));
        });
    }
}
