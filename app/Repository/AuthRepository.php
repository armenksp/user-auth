<?php

namespace App\Repository;

use App\Models\User;
use App\Models\UserLogged;
use App\Repository\Interfaces\AuthRepositoryInterfaces;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class AuthRepository implements AuthRepositoryInterfaces
{
    public function createUser($data): ?User
    {
        $userData = [
            'name' => $data['name'],
            'email' => $data['email'],
            'phone_number' => $data['phone_number'],
            'password' => Hash::make($data['password']),
        ];
        return User::create($userData);
    }

    public function checkUserEmail($userEmail)
    {
        return User::where('email', $userEmail)->first();
    }

    public function userLoggedTime($userId)
    {
        return UserLogged::insert(['user_id' => $userId, 'logged_date' => Carbon::now()->toDateTimeString()]);
    }

}

