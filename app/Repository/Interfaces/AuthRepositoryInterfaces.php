<?php

namespace App\Repository\Interfaces;

interface AuthRepositoryInterfaces
{

    public function createUser($data);

    public function checkUserEmail($userEmail);
}
