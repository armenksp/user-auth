<?php

namespace App\Repository\Interfaces;

interface OrderRepositoryInterfaces
{

    public function createOrder($data);

    public function updateOrder($orderId);

    public function deleteOrder($orderId);

    public function getAllOrders();
}
