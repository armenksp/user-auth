<?php

namespace App\Repository\Interfaces;

interface ShipmentRepositoryInterfaces
{

    public function shipment($data);
}
