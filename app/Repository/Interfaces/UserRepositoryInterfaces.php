<?php

namespace App\Repository\Interfaces;

interface UserRepositoryInterfaces
{
    public function getAllUsers();

    public function updateUser($userData);

}
