<?php

namespace App\Repository;

use App\Models\Order;
use App\Repository\Interfaces\OrderRepositoryInterfaces;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;


class OrderRepository implements OrderRepositoryInterfaces
{

    public function createOrder($data)
    {
        $createData = [
            'user_id' => auth('api')->user()->id,
            'order_date' => Carbon::now()->toDateTimeString(),
            'price' => $data['price'],
            'status_enum' => 'pending',
        ];
        return Order::insert($createData);
    }

    public function updateOrder($orderId)
    {
        return tap(Order::findOrFail($orderId))->update(['status_enum' => 'completed'])->fresh();
    }

    public function deleteOrder($orderId)
    {
        $task = Order::findOrFail($orderId);
        $result = $task->delete();
        if ($result) {
            return Order::all();
        }
    }


    public function getAllOrders()
    {
        return Order::get();
    }

}

