<?php

namespace App\Repository;


use App\Models\Shipment;
use App\Repository\Interfaces\ShipmentRepositoryInterfaces;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ShipmentRepository implements ShipmentRepositoryInterfaces
{

    public function shipment($data): bool
    {
        $insertData = [
            'order_id' => $data['orderId'],
            'shipment_date' => Carbon::now()->toDateTimeString()
        ];
        $result = Shipment::insert($insertData);
        if ($result) {
            DB::table('orders')->where('id', $data['orderId'])->update(['status_enum' => 'completed']);
            return true;
        }
        return false;
    }

}

