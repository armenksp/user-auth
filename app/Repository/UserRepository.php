<?php

namespace App\Repository;


use App\Models\User;
use App\Repository\Interfaces\UserRepositoryInterfaces;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterfaces
{

    public function getAllUsers()
    {
        return User::get();
    }

    public function updateUser($userData)
    {
        return User::where('id', $userData['id'])
            ->update([
                'name' => $userData['name'],
                'email' => $userData['email'],
                'phone_number' => $userData['phone_number'],
                'password' => Hash::make($userData['password']),
            ]);
    }
}

