<?php

namespace App\Services;


use App\Components\ImportDataAddress;
use App\Components\ImportDataAddressById;
use App\Components\ImportDataAddressByPostCode;
use App\Services\Interfaces\AddressServiceInterface;
use Illuminate\Support\Collection;


/**
 * Class UserService
 * @package App\Services
 */
class AddressService implements AddressServiceInterface
{

    public function getAddress($data): Collection
    {

        $top = $data['top'];

        $template = $data['template'];
        $import = new ImportDataAddress();
        $res = $import->client->request('GET',
            env('ADDRESS_URL') . 'autocomplete/term' . '?api-key=' . env('API_KEY')
            . '&top=' . $top . '&template=' . $template);
        $data = $res->getBody();

        return collect(json_decode($data));

    }

    public function getAddressById($addressId): Collection
    {
        $import = new ImportDataAddressById();
        $res = $import->client->request('GET', env('ADDRESS_URL') . 'get/' . $addressId . '?api-key=' . env('API_KEY'));
        $data = $res->getBody();
        return collect(json_decode($data));
    }

    public function getAddressByPostCode ($postCode): Collection
    {
        $import = new ImportDataAddressByPostCode();
        $res = $import->client->request('GET', env('ADDRESS_URL') . 'find/' . $postCode . '?api-key=' . env('API_KEY'));
        $data = $res->getBody();

        return collect(json_decode($data));
    }

}
