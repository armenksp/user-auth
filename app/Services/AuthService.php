<?php

namespace App\Services;

use App\Models\User;
use App\Repository\AuthRepository;
use App\Services\Interfaces\AuthServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

/**
 * Class AuthService
 * @package App\Services
 */
class AuthService implements AuthServiceInterface
{
    public function createUser($data): User
    {
        return (new AuthRepository)->createUser($data);
    }


    public function loginUser($userData): ?JsonResponse
    {
        if (!$token = auth()->attempt($userData)) {
            return response()->json(['message' => __('messages.Unauthorized')], 401);
        }
        $userInfo = (new AuthRepository)->checkUserEmail($userData['email']);
        if ($userInfo) {

            if (is_null($userInfo['email_verified_at'])) {
                return response()->json(['status' => 404, 'message' => __('messages.please verify email')]);
            }

            if (!Hash::check($userData['password'], $userInfo->password)) {
                return response()->json(['status' => 404, 'message' => __('messages.password dont match')]);
            }

            (new AuthRepository)->userLoggedTime($userInfo['id']);

            return $this->respondWithToken($token);

        }

        return response()->json(['status' => 404, 'message' => __('messages.no such user exists')]);

    }


    protected function respondWithToken($token): JsonResponse
    {
        return response()->json([
            'status' => 200,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

}
