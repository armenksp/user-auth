<?php

namespace App\Services\Interfaces;

interface AddressServiceInterface
{

    public function getAddress($data);

    public function getAddressById($addressId);

    public function getAddressByPostCode($postCode);

}
