<?php

namespace App\Services\Interfaces;

interface AuthServiceInterface
{
    public function createUser($data);

    public function loginUser($userData);
}
