<?php

namespace App\Services\Interfaces;

interface OrderServiceInterface
{
    public function createOrder($data);

    public function updateOrder($orderId);

    public function deleteOrder($orderId);

    public function getAllOrders();
}
