<?php

namespace App\Services\Interfaces;

interface ShipmentServiceInterface
{
    public function shipment($data);
}
