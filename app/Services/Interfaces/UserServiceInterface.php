<?php

namespace App\Services\Interfaces;

interface UserServiceInterface
{
    public function getAllUsers();

    public function updateUser($userData);
}
