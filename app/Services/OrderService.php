<?php

namespace App\Services;

use App\Repository\OrderRepository;
use App\Services\Interfaces\OrderServiceInterface;
use Illuminate\Database\Eloquent\Collection;


/**
 * Class AuthService
 * @package App\Services
 */
class OrderService implements OrderServiceInterface
{

    public function createOrder($data)
    {
        return (new OrderRepository)->createOrder($data);
    }

    public function updateOrder($orderId)
    {
        return (new OrderRepository)->updateOrder($orderId);
    }

    public function deleteOrder($orderId): ?Collection
    {
        return (new OrderRepository)->deleteOrder($orderId);
    }

    public function getAllOrders()
    {
        return (new OrderRepository)->getAllOrders();
    }

}
