<?php

namespace App\Services;

use App\Repository\ShipmentRepository;
use App\Services\Interfaces\ShipmentServiceInterface;


/**
 * Class ShipmentService
 * @package App\Services
 */
class ShipmentService implements ShipmentServiceInterface
{

    private ShipmentRepository $shipmentRepository;

    public function __construct(ShipmentRepository $shipmentRepository)
    {
        $this->shipmentRepository = $shipmentRepository;
    }

    public function shipment($data): bool
    {
        return $this->shipmentRepository->shipment($data);
    }

}
