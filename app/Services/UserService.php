<?php

namespace App\Services;


use App\Http\Resources\UserResource;
use App\Repository\UserRepository;
use App\Services\Interfaces\UserServiceInterface;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class UserService
 * @package App\Services
 */
class UserService implements UserServiceInterface
{

    public function getAllUsers(): AnonymousResourceCollection
    {
        $allUsers = (new UserRepository)->getAllUsers();
        return UserResource::collection($allUsers);
    }

    public function updateUser($userData)
    {
        return (new UserRepository)->updateUser($userData);
    }


}
