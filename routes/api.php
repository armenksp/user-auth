<?php

use App\Http\Controllers\AddressController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\ShipmentController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VerificationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/create', [AuthController::class, 'createUser'])->name('createUser');
Route::post('/login', [AuthController::class, 'login'])->name('login');

Route::get('email/verify/{id}', [VerificationController::class, 'verify'])->name('verification.verify');
Route::get('email/resend', [VerificationController::class, 'resend'])->name('verification.resend');

Route::get('/all-users', [UserController::class, 'getUsers'])->name('getUsers');
Route::post('/update', [UserController::class, 'updateUser'])->name('updateUser');
Route::post('/all-address', [AddressController::class, 'getAddress'])->name('getAddress');
Route::post('/address', [AddressController::class, 'getAddressById'])->name('getAddressById');
Route::post('/address-by-post-code', [AddressController::class, 'getAddressByPostCode'])->name('getAddressByPostCode');

Route::group(['middleware' => 'auth'], function () {
    Route::post('/create-order', [OrdersController::class, 'createOrder'])->name('createOrder');
    Route::post('/update-order', [OrdersController::class, 'updated'])->name('updated');
    Route::delete('/delete-order/{id}', [OrdersController::class, 'delete'])->name('delete');
    Route::get('/all-orders', [OrdersController::class, 'showOrders'])->name('showOrders');


    Route::post('/shipment', [ShipmentController::class, 'shipment'])->name('shipment');
});

