<?php

namespace Tests\Unit;

use App\Models\User;
use App\Services\AuthService;
use Tests\TestCase;

class AuthTest extends TestCase
{

    private AuthService $service;

    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->service = new AuthService();
    }


    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreateUser(): void
    {
        $userData = [
            'name' => randomUserName(),
            'email' => generateRandomString(),
            'phone_number' => generateRandomNumber(),
            'password' => randomPassword(),
        ];
        $this->service->createUser($userData);
        $this->assertTrue(true);
    }


    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testLoginUser(): void
    {
        $userData = [
            'email' => 'authtest@gmail.com',
            'password' => 'test333%%%test',
        ];
        $this->service->loginUser($userData);
        $this->assertTrue(true);
    }

    public function testVerifyEmailValidatesUser(): void
    {
        // TODO will return a random user
        $user = User::inRandomOrder()->first();

        $this->assertTrue(User::find($user['id'])->hasVerifiedEmail());
    }

}
